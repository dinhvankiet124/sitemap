package sitemap

import (
	"strings"
	"fmt"
)

const (
	slash = "/"
	empty = ""
)

type SiteMap map[string]*Node

type Node struct {
	Children SiteMap
	Depth    int
}

func NewSitemap() SiteMap {
	return make(SiteMap)
}

func (n *Node) Add(url string) {
	path := NormalizeUrl(url)
	if path == empty {
		return
	}

	head := n
	way := []*Node{head}
	for _, elem := range strings.Split(path, slash) {
		if head.Children == nil {
			head.Children = make(SiteMap)
		}
		node, ok := head.Children[elem]
		if !ok {
			node = &Node{}
			head.Children[elem] = node
		}
		head = node
		way = append(way, head)
	}

	depth := len(way) - 1
	for i := 0; i < len(way); i++ {
		if way[i].Depth < depth-i {
			way[i].Depth = depth - i
		}
	}
}

func (n *Node) DepthHard() int {
	depth := 0
	for _, node := range n.Children {
		if node.Depth > depth {
			depth = node.Depth
		}
	}
	return depth
}

func (n *Node) Pred(url string) (key, value string) {
	path := NormalizeUrl(url)
	if path == empty {
		return
	}

	if n.Depth <= 1 {
		value = url
		return
	}

	head := n
	elems := strings.Split(path, slash)
	for i, elem := range elems {
		node, ok := head.Children[elem]
		if !ok {
			key = strings.Join(elems[:i], slash)
			value = strings.Join(elems[i:], slash)
			return
		}
		if node.Depth == 0 {
			key = strings.Join(elems[:i], slash)
			value = strings.Join(elems[i:], slash)
			return
		} else if node.Depth == 1 {
			key = strings.Join(elems[:i+1], slash)
			value = strings.Join(elems[i+1:], slash)
			return
		}
		head = node
	}

	return
}

func NormalizeUrl(url string) string {
	if strings.Index(url, "http") != -1 {
		return empty
	}
	paramPos := strings.Index(url, "?")
	if paramPos != -1 {
		url = url[:paramPos]
	}
	thangPos := strings.Index(url, "#")
	if thangPos != -1 {
		url = url[:thangPos]
	}
	url = strings.Trim(url, "/ ")
	return url
}

func Visualize(n *Node, depth int) {
	if n.Children == nil {
		return
	}
	for child, node := range n.Children {
		for i := 0; i < depth; i++ {
			fmt.Print("-")
		}
		fmt.Println(fmt.Sprintf("Depth: %d, Node: %s", node.Depth, child))
		Visualize(node, depth+1)
	}
}
