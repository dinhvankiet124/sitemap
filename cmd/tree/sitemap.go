package main

import (
	"fmt"
	"os"
	"encoding/csv"
	"io"
	"strings"
	"gitlab.com/dinhvankiet124/url-rewrite/pkg/sitemap"
	"bufio"
)

func main() {
	f, err := os.Open("data.csv")
	if err != nil {
		panic(err)
	}
	defer f.Close()

	tsv := csv.NewReader(f)
	site := sitemap.NewSitemap()

	tsv.Read()

	fmt.Println("Start building")

	var urls []string
	for {
		record, err := tsv.Read()
		if err == io.EOF {
			break
		} else if err != nil {
			fmt.Println(err)
			continue
		}

		host := record[0]
		node, ok := site[host]
		if !ok {
			node = &sitemap.Node{}
			site[host] = node
		}

		url := strings.Replace(record[1], host, "", 1)

		node.Add(url)

		urls = append(urls, url)
	}

	node := site["https://shop.viettel.vn"]

	fmt.Println("Start visualize")
	fmt.Println("Depth", node.DepthHard())

	sitemap.Visualize(node, 0)

	sc := bufio.NewScanner(os.Stdin)
	for {
		fmt.Print("Enter a url: ")
		sc.Scan()
		url := sc.Text()

		key, value := node.Pred(url)
		fmt.Println("Key:", key, "Value:", value)
	}
}
